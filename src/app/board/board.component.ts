import { Component } from '@angular/core';
import { Cell } from "../cell/cell.model";

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.css']
})

export class BoardComponent {
  cells: Cell[] = [];
  tries = 0;
  winner = false;

  constructor() {
    this.getCells();
  }

  getCells() {
    this.cells = [];
    const randomIndex = Math.floor(Math.random() * 36) + 1;
    let content = false;
    for (let i = 0; i < 36; i++ ) {
      content = i + 1 == randomIndex; // <- непонял как это, IDE предложила упростить выражение :)
      this.cells.push(new Cell('close', content));
    }
  }

  onCellClick(index: number) {
    if (!this.winner) {
      this.cells[index].state = 'open';
      this.tries = this.tries + 1;
    }
    if (this.cells[index].content) {
      this.winner = true;
    }
  }

  onReset() {
    this.getCells();
    this.tries = 0;
    this.winner = false;
  }
}
