import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css'],
  host: {'class': 'cell'}
})
export class CellComponent {
  @Input() state = '';
  @Input() content = false;
}
